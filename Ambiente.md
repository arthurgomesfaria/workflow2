- Sistema Operacional

  - Debian >= 9

  - Red Hat

  - FreeBSD

    

- Bibliotecas necessárias

  - PHP >= 7.2.5

  - BCMath PHP Extension

  - Ctype PHP Extension

  - Fileinfo PHP extension

  - JSON PHP Extension

  - Mbstring PHP Extension

  - OpenSSL PHP Extension

  - PDO PHP Extension

  - Tokenizer PHP Extension

  - XML PHP Extension

  - JpegOptim

  - Optipng

  - Pngquant 2

  - SVGO

  - Gifsicle

  - cwebp
 
  - build-essential
  
  - checkinstall 
 
  - zlib1g-dev

    

- Banco de dados

  - MySQL >= 5.6

  - Redis

  - PostgreSQL

    

- Permissões de pasta

  - 777 - 664 - /var/www/html/servico/storage/app

  - chown www:data /var/www/html/servico

    

- Conexões externas
  - webservices.gov.esocial
  - npmregistry.com.br

