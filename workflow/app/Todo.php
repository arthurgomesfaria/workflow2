<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model 
{

    protected $table = 'todo';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'done',
    ];
}
