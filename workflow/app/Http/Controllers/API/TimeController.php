<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Carbon\Carbon;

class TimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [
            'datetime' => Carbon::now()->format('d-m-Y')
        ];

        return response()->json($response, 200);
    }
    public function teste () {
        $response = [
            'datetime' => Carbon::now()
        ];
        return response()->json($response, 200);
    }
}