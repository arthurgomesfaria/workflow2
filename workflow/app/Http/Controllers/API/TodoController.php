<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Carbon\Carbon;
use App\Todo;
use Validator;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Todo::all(), 200);
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);
        $tarefa = new Todo;
        $tarefa->title = $input['title'];
        $tarefa->description = array_key_exists('description', $input) ? $input['description'] : '';
        $tarefa->done = array_key_exists('done', $input) ? $input['done'] : false;
        $tarefa->save();
        return response()->json($tarefa);
    }

    public function show($id) {
        $tarefa = Todo::findOrFail($id);
        return response()->json($tarefa, 200);
    }

    public function update(Request $request, $id) {
        $input = $request->all();

        $tarefa = Todo::findOrFail($id);
        $tarefa->title = array_key_exists('title', $input) ? $input['title'] : '';
        $tarefa->description = array_key_exists('description', $input) ? $input['description'] : '';
        $tarefa->done = array_key_exists('done', $input) ? $input['done'] : false;
        $tarefa->save();
        return response()->json($tarefa);
    }

    public function destroy($id)
    {
        $tarefa = Todo::findOrFail($id);
        $tarefa->delete();
        return response()->json(['success' => 'true', 'message' => 'Successfully deleted']); 
    }
}