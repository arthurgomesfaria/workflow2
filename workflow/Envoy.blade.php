@servers(['web' => 'deployer@arthur-faria-1.paiza-user-free.cloud -p 52800', 'prod' => 'deployer@arthur-faria-1.paiza-user-free.cloud -p 52800'])

@setup
    $repository = 'git@gitlab.com:arthurgomesfaria/workflow.git';
    $releases_dir = '/var/www/html/app/releases';
    $app_dir = '/var/www/html/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy_swarm'  ['on' => 'prod'])
    clone_repository
    update_swarm
@endstory

@story('deploy'['web' => 'prod'])
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('update_swarm')
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    docker stack deploy -c composer.yml --prune --with-reigstry-auth
@endtask

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}/workflow
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/workflow/storage
    ln -nfs {{ $app_dir }}/workflow/storage {{ $new_release_dir }}/workflow/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/workflow/.env {{ $new_release_dir }}/workflow/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }}/workflow {{ $app_dir }}/current
@endtask
