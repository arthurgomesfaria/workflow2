<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Arthurfaria\Greeter\Greeter;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware([])->group( function () {
    Route::get('server-time', 'API\TimeController@index');
    Route::get('meridian-time', 'API\TimeController@teste');
    Route::resource('tarefas', 'API\TodoController');
});

Route::middleware([])->get('/greet/{user}', function (Request $request) {
    $Greetr = new Greeter();
    return $Greetr->greet($request->user);
});