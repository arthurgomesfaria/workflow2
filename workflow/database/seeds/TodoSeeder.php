<?php

use Illuminate\Database\Seeder;
use App\Todo;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tarefa = Todo::firstOrNew(['title' => 'Primeira Tarefa']);
        if (!$tarefa->exists) {
            $tarefa->fill([
                'title' => 'Primeira Tarefa',
                'description' => 'Minha primeira tarefa',
                'done' => false
            ])->save();
        }

        $tarefa = Todo::firstOrNew(['title' => 'Segunda Tarefa']);
        if (!$tarefa->exists) {
            $tarefa->fill([
                'title' => 'Segunda Tarefa',
                'description' => 'Minha segunda tarefa',
                'done' => true
            ])->save();
        }
    }
}
